﻿using Microsoft.EntityFrameworkCore;
using silos.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace silos.Db
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { set; get; }

        public DbSet<Access> AccessList { set; get; }

        public DbSet<Department> Departments { set; get; }

        public DbSet<Position> Positions { set; get; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=testdb;Username=postgres;Password=123456");
        }
    }
}
