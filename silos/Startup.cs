using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using silos.Db;
using silos.Entities;

namespace silos
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            /*
            * �������� ��������� �������� � ����
            */
            using (ApplicationContext context = new ApplicationContext())
            {
                //Department dep1 = new Department() { Id = 1, DisplayName = "����������� ���������� �������" };
                //Department dep2 = new Department() { Id = 2, DisplayName = "����������� ���������� �������" };

                //context.Departments.Add(dep1);
                //context.Departments.Add(dep2);

                //Position pos1 = new Position() { Id = 1, DisplayName = "�������������" };
                //Position pos2 = new Position() { Id = 2, DisplayName = "��������" };
                //Position pos3 = new Position() { Id = 3, DisplayName = "���������" };
                //Position pos4 = new Position() { Id = 4, DisplayName = "���������" };

                //context.Positions.Add(pos1);
                //context.Positions.Add(pos2);
                //context.Positions.Add(pos3);
                //context.Positions.Add(pos4);
                
                //User us1 = new User() { Id = 1, Position = pos1, Department = dep1, FirstName = "Imya", LastName = "Familia", MiddleName = "Otchestvo", Sex = "male" };

                //context.Users.Add(us1);

                context.SaveChanges();
            }
            
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
