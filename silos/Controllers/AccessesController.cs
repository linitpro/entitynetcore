﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using silos.Db;
using silos.Entities;

namespace silos.Controllers
{
    [Produces("application/json")]
    [Route("api/Accesses")]
    public class AccessesController : Controller
    {
        private readonly ApplicationContext _context;

        public AccessesController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: api/Accesses
        [HttpGet]
        public IEnumerable<Access> GetAccessList()
        {
            return _context.AccessList;
        }

        // GET: api/Accesses/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAccess([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var access = await _context.AccessList.SingleOrDefaultAsync(m => m.Id == id);

            if (access == null)
            {
                return NotFound();
            }

            return Ok(access);
        }

        // PUT: api/Accesses/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccess([FromRoute] int id, [FromBody] Access access)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != access.Id)
            {
                return BadRequest();
            }

            _context.Entry(access).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccessExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Accesses
        [HttpPost]
        public async Task<IActionResult> PostAccess([FromBody] Access access)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.AccessList.Add(access);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAccess", new { id = access.Id }, access);
        }

        // DELETE: api/Accesses/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccess([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var access = await _context.AccessList.SingleOrDefaultAsync(m => m.Id == id);
            if (access == null)
            {
                return NotFound();
            }

            _context.AccessList.Remove(access);
            await _context.SaveChangesAsync();

            return Ok(access);
        }

        private bool AccessExists(int id)
        {
            return _context.AccessList.Any(e => e.Id == id);
        }
    }
}