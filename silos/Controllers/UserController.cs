﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using silos.Db;
using silos.Dto;
using silos.Dto.MapperProfile;
using silos.Entities;

namespace silos.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private IMapper _mapper;
        private ApplicationContext _context;

        public UserController()
        {
            var confif = new MapperConfiguration(cfg => cfg.AddProfile(typeof(UserProfile)));
            _context = new ApplicationContext();
            _mapper = confif.CreateMapper();
        }

        // GET: api/User
        /// <summary>
        /// Возвращает список всех пользователей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<UserDto> Get()
        {
            var model = _mapper.Map<List<UserDto>>(_context.Users);
            return model;
        }

        // GET: api/User/5
        /// <summary>
        /// Возвращает пользователя по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "Get")]
        public UserDto Get(int id)
        {
            var model = _mapper.Map<UserDto>(_context.Users.FirstOrDefault(x=>x.Id == id));
            return model;
        }
        
        // POST: api/User
        [HttpPost]
        public bool Post([FromBody]UserDto userDto)
        {
            var model = _mapper.Map<User>(userDto);
            _context.Users.Add(model);
            var result = _context.SaveChanges();
            return result == 1 ? true : false;
        }
        
        // PUT: api/User/5
        /// <summary>
        /// Изменяет пользователя
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <param name="value"></param>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]UserDto userDto)
        {
            var model = _mapper.Map<User>(userDto);
            _context.Users.Update(model);
            _context.SaveChanges();
        }
        
        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Удаляет пользователя по его идентификатору
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var user = _context.Users.FirstOrDefault(x=>x.Id == id);
            _context.Users.Remove(user);
            _context.SaveChanges();
        }
    }
}
