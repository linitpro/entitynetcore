﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace silos.Entities
{
    /// <summary>
    /// Модель должностей
    /// </summary>
    public class Position
    {
        public int Id { set; get; }

        public string DisplayName { set; get; }
    }
}
