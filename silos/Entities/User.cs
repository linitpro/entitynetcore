﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace silos.Entities
{
    /// <summary>
    /// Пользователь
    /// </summary>
	public class User
	{
        /// <summary>
        /// Id пользователя
        /// </summary>
        public int Id { set; get; }
        /// <summary>
        /// Имя
        /// </summary>
		public string FirstName { set; get; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { set; get; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string  LastName { set; get; }
        /// <summary>
        /// Пол
        /// </summary>
        public string Sex { set; get; }
        /// <summary>
        /// Ссылка на фото
        /// </summary>
        public string Photo { set; get; }

        public int DepartmentId { set; get; }
        /// <summary>
        /// Департамент
        /// </summary>
        public Department Department { set; get; }

        public int PositionId { set; get; }
        /// <summary>
        /// Должность
        /// </summary>
        public Position Position { set; get; }

        /// <summary>
        /// Список доступа
        /// </summary>
        public List<Access> AccessList { set; get; }

    }
}