﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace silos.Entities
{
    public class Department
    {
        public int Id { set; get; }

        public string DisplayName { set; get; }
    }
}
