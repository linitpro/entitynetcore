﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace silos.Entities
{
    /// <summary>
    /// Доступ
    /// </summary>
    public class Access
    {
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public int Id { set; get; }
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { set; get; }
        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { set; get; }
        /// <summary>
        /// Отобрадаемое название Доступа
        /// </summary>
        public string DisplayName { set; get; }
        /// <summary>
        /// Значение
        /// </summary>
        public bool Value { set; get; }
    }
}
