﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace silos.Dto
{
    public class AccessDto
    {
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public int Id { set; get; }

        /// <summary>
        /// Отобрадаемое название Доступа
        /// </summary>
        public string DisplayName { set; get; }

        /// <summary>
        /// Значение
        /// </summary>
        public bool Value { set; get; }
    }
}
