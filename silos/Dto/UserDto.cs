﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace silos.Dto
{
    public class UserDto
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public int Id { set; get; }
        /// <summary>
        /// Имя
        /// </summary>
		public string FirstName { set; get; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { set; get; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { set; get; }
        /// <summary>
        /// Пол
        /// </summary>
        public string Sex { set; get; }
        /// <summary>
        /// Ссылка на фото
        /// </summary>
        public string Photo { set; get; }

        public int DepartmentId { set; get; }
        /// <summary>
        /// Департамент
        /// </summary>
        public DepartmentDto Department { set; get; }
        
        public int PositionId { set; get; }
        /// <summary>
        /// Должность
        /// </summary>
        public PositionDto Position { set; get; }

        /// <summary>
        /// Список доступа
        /// </summary>
        public List<AccessDto> AccessList { set; get; }
    }
}
