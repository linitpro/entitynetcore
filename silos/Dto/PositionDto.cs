﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace silos.Dto
{
    public class PositionDto
    {
        public int Id { set; get; }

        public string DisplayName { set; get; }

    }
}
